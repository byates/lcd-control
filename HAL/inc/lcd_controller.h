#ifndef __LCD_CONTROLLER_H__
#define __LCD_CONTROLLER_H__

#include alt_types.h

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */

#define LCD_CONTROLLER_INSTANCE(name, dev) extern int alt_no_storage
#define LCD_CONTROLLER_INIT(name, dev) while (0)

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __LED_BLINKER_H__ */
