package require -exact qsys 15.0

#
# module lcd_controller.sv
#
set_module_property DESCRIPTION ""
set_module_property NAME LCD_Controller
set_module_property VERSION 1.0
set_module_property INTERNAL false
set_module_property OPAQUE_ADDRESS_MAP true
set_module_property GROUP Custom_Modules
set_module_property AUTHOR "Austin Yates"
set_module_property DISPLAY_NAME LCD_Controller
set_module_property INSTANTIATE_IN_SYSTEM_MODULE true
set_module_property EDITABLE false
set_module_property REPORT_TO_TALKBACK false
set_module_property ALLOW_GREYBOX_GENERATION false
set_module_property REPORT_HIERARCHY false

#
# file sets
#
add_fileset QUARTUS_SYNTH QUARTUS_SYNTH "" ""

set_fileset_property QUARTUS_SYNTH TOP_LEVEL lcd_controller
set_fileset_property QUARTUS_SYNTH ENABLE_RELATIVE_INCLUDE_PATHS false

add_fileset_file std_types_pkg.sv SYSTEM_VERILOG PATH rtl/std_types_pkg.sv
add_fileset_file std_functions_pkg.sv SYSTEM_VERILOG PATH rtl/std_functions_pkg.sv
add_fileset_file lcd_controller_pkg.sv SYSTEM_VERILOG PATH rtl/lcd_controller_pkg.sv
add_fileset_file fifo_altera_1clk_ft.sv SYSTEM_VERILOG PATH rtl/fifo_altera_1clk_ft.sv
add_fileset_file lcd_ctrl_cmd_processor.sv SYSTEM_VERILOG PATH rtl/lcd_ctrl_cmd_processor.sv
add_fileset_file lcd_controller.sv SYSTEM_VERILOG PATH rtl/lcd_controller.sv

#
# parameters
#
add_parameter PARM_FIFO_DEPTH INTEGER 256

set_parameter_property PARM_FIFO_DEPTH DISPLAY_NAME "FIFO Depth"
set_parameter_property PARM_FIFO_DEPTH UNITS None
set_parameter_property PARM_FIFO_DEPTH HDL_PARAMETER true

#
# display items
#

#
# connection point clock
#
add_interface sys_clk clock end

set_interface_property sys_clk clockRate 100000000
set_interface_property sys_clk ENABLED true

add_interface_port sys_clk sys_clk_in clk Input 1

#
# connection point reset
#
add_interface sys_rst reset end

set_interface_property sys_rst associatedClock sys_clk
set_interface_property sys_rst ENABLED true

add_interface_port sys_rst sys_reset_in reset Input 1

#
# connection point avalon_slave
#
add_interface avalon_slave avalon end

set_interface_property avalon_slave addressUnits WORDS
set_interface_property avalon_slave associatedClock sys_clk
set_interface_property avalon_slave associatedReset sys_rst
set_interface_property avalon_slave bitsPerSymbol 8
set_interface_property avalon_slave burstOnBurstBoundariesOnly false
set_interface_property avalon_slave burstcountUnits WORDS
set_interface_property avalon_slave explicitAddressSpan 0
set_interface_property avalon_slave holdTime 0
set_interface_property avalon_slave linewrapBursts false
set_interface_property avalon_slave maximumPendingReadTransactions 0
set_interface_property avalon_slave readLatency 0
set_interface_property avalon_slave readWaitTime 1
set_interface_property avalon_slave setupTime 0
set_interface_property avalon_slave timingUnits Cycles
set_interface_property avalon_slave writeWaitTime 0
set_interface_property avalon_slave ENABLED true

add_interface_port avalon_slave avs_address_in address Input 3
add_interface_port avalon_slave avs_write_in write Input 1
add_interface_port avalon_slave avs_writedata_in writedata Input 32
add_interface_port avalon_slave avs_read_in read Input 1
add_interface_port avalon_slave avs_readdata_out readdata Output 32

#
# connection point lcd_controller
#
add_interface lcd_spi conduit end

set_interface_property lcd_spi associatedClock sys_clk
set_interface_property lcd_spi associatedReset sys_rst

add_interface_port lcd_spi lcd_data_io Data Inout 8
add_interface_port lcd_spi lcd_cs_n_out CS  Output 1
add_interface_port lcd_spi lcd_d_c_out d_c Output 1
add_interface_port lcd_spi lcd_rst_n_out rst Output 1

#
# connection point interrupt
#
add_interface irq interrupt sender

set_interface_property irq associatedAddressablePoint avalon_slave
set_interface_property irq associatedClock sys_clk
set_interface_property irq associatedReset sys_rst

add_interface_port irq interrupt_out irq Output 1
