#ifndef __LCD_CONTROLLER_REGS_H__
#define __LCD_CONTROLLER_REGS_H__

#include <io.h>

#define IOADDR_FIFO_DATA(base)      __IO_CALC_ADDRESS_NATIVE(base, 0)
#define IORD_FIFO_DATA(base)        IORD(base, 0)
#define IOWR_FIFO_DATA(base, data)  IOWR(base, 0, data)

#define IOADDR_FIFO_COUNTS(base)    __IO_CALC_ADDRESS_NATIVE(base, 1)
#define IORD_FIFO_COUNTS(base)      IORD(base, 1)

#define IOADDR_IRQ_STATUS(base)     __IO_CALC_ADDRESS_NATIVE(base, 2)
#define IORD_IRQ_STATUS(base)       IORD(base, 2)

#define IOADDR_IRQ_SET_EN(base)     __IO_CALC_ADDRESS_NATIVE(base, 3)
#define IOWR_IRQ_SET_EN(base, data) IOWR(base, 3, data)

#define IOADDR_IRQ_CLR_EN(base)     __IO_CALC_ADDRESS_NATIVE(base, 4)
#define IOWR_IRQ_CLR_EN(base, data) IOWR(base, 4, data)

#endif
