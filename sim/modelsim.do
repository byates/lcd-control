alias rf "restart -f"

set QUARTUS $env(QUARTUS_ROOTDIR)

set sim_compiler vlog
set sim_options "vlog.opt"

# Define paths to command modules
set working_library __modelsim_lib__
set working_library_path "./$working_library"

set iq_modules_path "../.."
set iq_modules_packages_path "$iq_modules_path/LCDcontrol/rtl"
set iq_tb_utilities_path "$iq_modules_path/LCDcontrol/sim"


vlib $working_library_path
vmap $working_library $working_library_path

# Modules
vlog -f vlog.opt    \
    $iq_modules_packages_path/std_types_pkg.sv                      \
    $iq_modules_packages_path/std_functions_pkg.sv                  \

# standard TB stuff
vlog -f vlog.opt    \
    $iq_tb_utilities_path/tb_reset_gen.sv             \
    $iq_tb_utilities_path/tb_clock_gen.sv             \

# lcd_controller
source $iq_modules_path/LCDcontrol/sim/lcd_controller.do

# TB stuff
vlog -f vlog.opt    \
    $iq_modules_path/LCDcontrol/sim/tb_lcd_controller.sv

if {($argc == 0)} {
    vsim -t 1fs +nowarnTSCALE -suppress 8233 +notimingchecks ${working_library}.tb_lcd_controller -L $working_library -L altera_mf_ver
    }
