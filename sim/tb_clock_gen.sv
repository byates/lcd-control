// ############################################################################
// File          :  tb_reset_gen.v
// Author(s)     :  Brent Yates
// Email         :  brent.yates@ineoquest.com
// Project       :  N/A
// Organization  :  IneoQuest Technologies Inc.
// Creation Date :  20 Dec 2005
// Description   :  Simple reset generator module for test benches
// ############################################################################
// Functional Description:
//
//
// ############################################################################
// Exceptions:
// ############################################################################
// Instantiated Blocks:
// ############################################################################
// Process/Technology specific information:
// ############################################################################
//     ##################################################################
//     #                                                                #
//     #   Copyright (C) 2005 by                                        #
//     #   IneoQuest Technologies Inc.                                  #
//     #   All rights reserved.                                         #
//     #                                                                #
//     #   This document contains proprietary data and is not to be     #
//     #   used, copied, reproduced, stored in a retrieval system,      #
//     #   transmitted, distributed or divulged to unauthorized         #
//     #   persons in whole or in part, without proper authorization    #
//     #   from IneoQuest Technologies Inc.  This information is the    #
//     #   property of IneoQuest Technologies Inc with all rights       #
//     #   reserved.                                                    #
//     #                                                                #
//     ##################################################################

import	std_types_pkg::*;
import	std_functions_pkg::*;

// -------------------------------------------------------------------------
// Module portlist
// -------------------------------------------------------------------------
module tb_clock_gen
    // Module parameters
    #(
        parameter real FREQUENCY_MHZ = 50.0,
        parameter real STARTUP_DELAY_NS = 98
    )
    // Module ports
    (
    // Output clock signal based on the period paramter.
    output	logic	clock_out,
    // 90 deg phase shift
    output	logic	clock_90_out
    );

timeunit 1ns;
timeprecision 1ps;

localparam time HALF_PERIOD_PS = ((1e6 / FREQUENCY_MHZ ) / 2.0);
localparam time QTR_PERIOD_PS = (HALF_PERIOD_PS / 2.0);

initial
    begin : INIT_ClockGeneration
    clock_out = 1'b1;
    #(STARTUP_DELAY_NS*1ns);
    // Keep clocking forever...
    forever
        begin
        clock_out = 1'b1;
        #(HALF_PERIOD_PS*1ps);
        clock_out = 1'b0;
        #(HALF_PERIOD_PS*1ps);
        end
    end	// end reset generation process

assign #(QTR_PERIOD_PS*1ps) clock_90_out = clock_out;

endmodule
