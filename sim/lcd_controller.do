set SOURCE_LOC $iq_modules_path/LCDcontrol/rtl
if {[info exists sim_compiler] == 0} {set sim_compiler vlog}
if {[info exists sim_options] == 0} {set sim_options "vlog.opt"}

$sim_compiler -f $sim_options    \
    "$SOURCE_LOC/lcd_controller_pkg.sv"           \
    "$SOURCE_LOC/lcd_ctrl_cmd_processor.sv"       \
    "$SOURCE_LOC/fifo_altera_1clk_ft.sv"          \
    "$SOURCE_LOC/lcd_controller.sv"               \
