onerror {resume}
quietly set dataset_list [list sim vsim]
if {[catch {datasetcheck $dataset_list}]} {abort}
quietly WaveActivateNextPane {} 0
add wave -noupdate -group tb_top sim:/tb_lcd_controller/sys_clock
add wave -noupdate -group tb_top sim:/tb_lcd_controller/sys_reset
add wave -noupdate -group tb_top -group SPI sim:/tb_lcd_controller/hps_lcm_spim_clk
add wave -noupdate -group tb_top -group SPI sim:/tb_lcd_controller/hps_lcm_spim_mosi
add wave -noupdate -group tb_top -group SPI sim:/tb_lcd_controller/hps_lcm_spim_ss
add wave -noupdate -group tb_top -group SPI sim:/tb_lcd_controller/hps_lcm_d_c
add wave -noupdate -group tb_top -group SPI sim:/tb_lcd_controller/hps_lcm_rst_n
add wave -noupdate -group tb_top -expand -group Avalon sim:/tb_lcd_controller/avs_address
add wave -noupdate -group tb_top -expand -group Avalon sim:/tb_lcd_controller/avs_read
add wave -noupdate -group tb_top -expand -group Avalon -radix hexadecimal sim:/tb_lcd_controller/avs_writedata
add wave -noupdate -group tb_top -expand -group Avalon sim:/tb_lcd_controller/avs_write
add wave -noupdate -group tb_top -expand -group Avalon -radix hexadecimal sim:/tb_lcd_controller/avs_readdata
add wave -noupdate -group tb_top sim:/tb_lcd_controller/irq_signal
add wave -noupdate -group tb_top -radix hexadecimal sim:/tb_lcd_controller/value
add wave -noupdate -group tb_top -radix hexadecimal sim:/tb_lcd_controller/lcd_data
add wave -noupdate -group lcd_contoller_top sim:/tb_lcd_controller/dut_inst/PARM_FIFO_DEPTH
add wave -noupdate -group lcd_contoller_top sim:/tb_lcd_controller/dut_inst/sys_clk_in
add wave -noupdate -group lcd_contoller_top sim:/tb_lcd_controller/dut_inst/sys_reset_in
add wave -noupdate -group lcd_contoller_top sim:/tb_lcd_controller/dut_inst/interrupt_out
add wave -noupdate -group lcd_contoller_top -group Spi sim:/tb_lcd_controller/dut_inst/hps_lcm_spim_clk_out
add wave -noupdate -group lcd_contoller_top -group Spi sim:/tb_lcd_controller/dut_inst/hps_lcm_spim_mosi_out
add wave -noupdate -group lcd_contoller_top -group Spi sim:/tb_lcd_controller/dut_inst/hps_lcm_spim_ss_out
add wave -noupdate -group lcd_contoller_top -group Spi sim:/tb_lcd_controller/dut_inst/hps_lcm_d_c_out
add wave -noupdate -group lcd_contoller_top -group Spi sim:/tb_lcd_controller/dut_inst/hps_lcm_rst_n_out
add wave -noupdate -group lcd_contoller_top -group Avalon sim:/tb_lcd_controller/dut_inst/avs_address_in
add wave -noupdate -group lcd_contoller_top -group Avalon sim:/tb_lcd_controller/dut_inst/avs_read_in
add wave -noupdate -group lcd_contoller_top -group Avalon sim:/tb_lcd_controller/dut_inst/avs_read_pulse
add wave -noupdate -group lcd_contoller_top -group Avalon sim:/tb_lcd_controller/dut_inst/avs_read_pulse_reg
add wave -noupdate -group lcd_contoller_top -group Avalon -radix hexadecimal -childformat {{{/tb_lcd_controller/dut_inst/avs_readdata_out[31]} -radix hexadecimal} {{/tb_lcd_controller/dut_inst/avs_readdata_out[30]} -radix hexadecimal} {{/tb_lcd_controller/dut_inst/avs_readdata_out[29]} -radix hexadecimal} {{/tb_lcd_controller/dut_inst/avs_readdata_out[28]} -radix hexadecimal} {{/tb_lcd_controller/dut_inst/avs_readdata_out[27]} -radix hexadecimal} {{/tb_lcd_controller/dut_inst/avs_readdata_out[26]} -radix hexadecimal} {{/tb_lcd_controller/dut_inst/avs_readdata_out[25]} -radix hexadecimal} {{/tb_lcd_controller/dut_inst/avs_readdata_out[24]} -radix hexadecimal} {{/tb_lcd_controller/dut_inst/avs_readdata_out[23]} -radix hexadecimal} {{/tb_lcd_controller/dut_inst/avs_readdata_out[22]} -radix hexadecimal} {{/tb_lcd_controller/dut_inst/avs_readdata_out[21]} -radix hexadecimal} {{/tb_lcd_controller/dut_inst/avs_readdata_out[20]} -radix hexadecimal} {{/tb_lcd_controller/dut_inst/avs_readdata_out[19]} -radix hexadecimal} {{/tb_lcd_controller/dut_inst/avs_readdata_out[18]} -radix hexadecimal} {{/tb_lcd_controller/dut_inst/avs_readdata_out[17]} -radix hexadecimal} {{/tb_lcd_controller/dut_inst/avs_readdata_out[16]} -radix hexadecimal} {{/tb_lcd_controller/dut_inst/avs_readdata_out[15]} -radix hexadecimal} {{/tb_lcd_controller/dut_inst/avs_readdata_out[14]} -radix hexadecimal} {{/tb_lcd_controller/dut_inst/avs_readdata_out[13]} -radix hexadecimal} {{/tb_lcd_controller/dut_inst/avs_readdata_out[12]} -radix hexadecimal} {{/tb_lcd_controller/dut_inst/avs_readdata_out[11]} -radix hexadecimal} {{/tb_lcd_controller/dut_inst/avs_readdata_out[10]} -radix hexadecimal} {{/tb_lcd_controller/dut_inst/avs_readdata_out[9]} -radix hexadecimal} {{/tb_lcd_controller/dut_inst/avs_readdata_out[8]} -radix hexadecimal} {{/tb_lcd_controller/dut_inst/avs_readdata_out[7]} -radix hexadecimal} {{/tb_lcd_controller/dut_inst/avs_readdata_out[6]} -radix hexadecimal} {{/tb_lcd_controller/dut_inst/avs_readdata_out[5]} -radix hexadecimal} {{/tb_lcd_controller/dut_inst/avs_readdata_out[4]} -radix hexadecimal} {{/tb_lcd_controller/dut_inst/avs_readdata_out[3]} -radix hexadecimal} {{/tb_lcd_controller/dut_inst/avs_readdata_out[2]} -radix hexadecimal} {{/tb_lcd_controller/dut_inst/avs_readdata_out[1]} -radix hexadecimal} {{/tb_lcd_controller/dut_inst/avs_readdata_out[0]} -radix hexadecimal}} -subitemconfig {{/tb_lcd_controller/dut_inst/avs_readdata_out[31]} {-height 15 -radix hexadecimal} {/tb_lcd_controller/dut_inst/avs_readdata_out[30]} {-height 15 -radix hexadecimal} {/tb_lcd_controller/dut_inst/avs_readdata_out[29]} {-height 15 -radix hexadecimal} {/tb_lcd_controller/dut_inst/avs_readdata_out[28]} {-height 15 -radix hexadecimal} {/tb_lcd_controller/dut_inst/avs_readdata_out[27]} {-height 15 -radix hexadecimal} {/tb_lcd_controller/dut_inst/avs_readdata_out[26]} {-height 15 -radix hexadecimal} {/tb_lcd_controller/dut_inst/avs_readdata_out[25]} {-height 15 -radix hexadecimal} {/tb_lcd_controller/dut_inst/avs_readdata_out[24]} {-height 15 -radix hexadecimal} {/tb_lcd_controller/dut_inst/avs_readdata_out[23]} {-height 15 -radix hexadecimal} {/tb_lcd_controller/dut_inst/avs_readdata_out[22]} {-height 15 -radix hexadecimal} {/tb_lcd_controller/dut_inst/avs_readdata_out[21]} {-height 15 -radix hexadecimal} {/tb_lcd_controller/dut_inst/avs_readdata_out[20]} {-height 15 -radix hexadecimal} {/tb_lcd_controller/dut_inst/avs_readdata_out[19]} {-height 15 -radix hexadecimal} {/tb_lcd_controller/dut_inst/avs_readdata_out[18]} {-height 15 -radix hexadecimal} {/tb_lcd_controller/dut_inst/avs_readdata_out[17]} {-height 15 -radix hexadecimal} {/tb_lcd_controller/dut_inst/avs_readdata_out[16]} {-height 15 -radix hexadecimal} {/tb_lcd_controller/dut_inst/avs_readdata_out[15]} {-height 15 -radix hexadecimal} {/tb_lcd_controller/dut_inst/avs_readdata_out[14]} {-height 15 -radix hexadecimal} {/tb_lcd_controller/dut_inst/avs_readdata_out[13]} {-height 15 -radix hexadecimal} {/tb_lcd_controller/dut_inst/avs_readdata_out[12]} {-height 15 -radix hexadecimal} {/tb_lcd_controller/dut_inst/avs_readdata_out[11]} {-height 15 -radix hexadecimal} {/tb_lcd_controller/dut_inst/avs_readdata_out[10]} {-height 15 -radix hexadecimal} {/tb_lcd_controller/dut_inst/avs_readdata_out[9]} {-height 15 -radix hexadecimal} {/tb_lcd_controller/dut_inst/avs_readdata_out[8]} {-height 15 -radix hexadecimal} {/tb_lcd_controller/dut_inst/avs_readdata_out[7]} {-height 15 -radix hexadecimal} {/tb_lcd_controller/dut_inst/avs_readdata_out[6]} {-height 15 -radix hexadecimal} {/tb_lcd_controller/dut_inst/avs_readdata_out[5]} {-height 15 -radix hexadecimal} {/tb_lcd_controller/dut_inst/avs_readdata_out[4]} {-height 15 -radix hexadecimal} {/tb_lcd_controller/dut_inst/avs_readdata_out[3]} {-height 15 -radix hexadecimal} {/tb_lcd_controller/dut_inst/avs_readdata_out[2]} {-height 15 -radix hexadecimal} {/tb_lcd_controller/dut_inst/avs_readdata_out[1]} {-height 15 -radix hexadecimal} {/tb_lcd_controller/dut_inst/avs_readdata_out[0]} {-height 15 -radix hexadecimal}} sim:/tb_lcd_controller/dut_inst/avs_readdata_out
add wave -noupdate -group lcd_contoller_top -group Avalon sim:/tb_lcd_controller/dut_inst/avs_write_in
add wave -noupdate -group lcd_contoller_top -group Avalon -radix hexadecimal sim:/tb_lcd_controller/dut_inst/avs_writedata_in
add wave -noupdate -group lcd_contoller_top -group Avalon -radix hexadecimal sim:/tb_lcd_controller/dut_inst/avs_writedata_reg
add wave -noupdate -group lcd_contoller_top sim:/tb_lcd_controller/dut_inst/l2h_rd_en_reg
add wave -noupdate -group lcd_contoller_top sim:/tb_lcd_controller/dut_inst/l2h_rd_data_avail
add wave -noupdate -group lcd_contoller_top -radix unsigned sim:/tb_lcd_controller/dut_inst/l2h_rd_count
add wave -noupdate -group lcd_contoller_top -radix hexadecimal sim:/tb_lcd_controller/dut_inst/l2h_rd_data
add wave -noupdate -group lcd_contoller_top -radix hexadecimal sim:/tb_lcd_controller/dut_inst/l2h_wr_data_reg
add wave -noupdate -group lcd_contoller_top sim:/tb_lcd_controller/dut_inst/l2h_wr_ready
add wave -noupdate -group lcd_contoller_top sim:/tb_lcd_controller/dut_inst/l2h_wr_en_reg
add wave -noupdate -group lcd_contoller_top sim:/tb_lcd_controller/dut_inst/h2l_wr_en_reg
add wave -noupdate -group lcd_contoller_top -radix hexadecimal sim:/tb_lcd_controller/dut_inst/irq_en_reg
add wave -noupdate -group lcd_contoller_top -radix hexadecimal sim:/tb_lcd_controller/dut_inst/irq_stat_reg
add wave -noupdate -group lcd_contoller_top -radix unsigned -childformat {{/tb_lcd_controller/dut_inst/fifo_counts.wr_freespace -radix unsigned} {/tb_lcd_controller/dut_inst/fifo_counts.rd_count -radix unsigned}} -expand -subitemconfig {/tb_lcd_controller/dut_inst/fifo_counts.wr_freespace {-height 15 -radix unsigned} /tb_lcd_controller/dut_inst/fifo_counts.rd_count {-height 15 -radix unsigned}} sim:/tb_lcd_controller/dut_inst/fifo_counts
add wave -noupdate -group lcd_cmd_processor sim:/tb_lcd_controller/dut_inst/lcd_ctrl_cmd_processor_inst/PARM_FIFO_DEPTH
add wave -noupdate -group lcd_cmd_processor sim:/tb_lcd_controller/dut_inst/lcd_ctrl_cmd_processor_inst/lcd_rd_en_reg
add wave -noupdate -group lcd_cmd_processor sim:/tb_lcd_controller/dut_inst/lcd_ctrl_cmd_processor_inst/lcd_rd_data_avail
add wave -noupdate -group lcd_cmd_processor -radix hexadecimal sim:/tb_lcd_controller/dut_inst/lcd_ctrl_cmd_processor_inst/lcd_rd_data
add wave -noupdate -group lcd_cmd_processor -radix hexadecimal sim:/tb_lcd_controller/dut_inst/lcd_ctrl_cmd_processor_inst/lcd_rd_data_reg
add wave -noupdate -group lcd_cmd_processor sim:/tb_lcd_controller/dut_inst/lcd_ctrl_cmd_processor_inst/sys_clk_in
add wave -noupdate -group lcd_cmd_processor sim:/tb_lcd_controller/dut_inst/lcd_ctrl_cmd_processor_inst/sys_reset_in
add wave -noupdate -group lcd_cmd_processor sim:/tb_lcd_controller/dut_inst/lcd_ctrl_cmd_processor_inst/cmd_wr_en_in
add wave -noupdate -group lcd_cmd_processor -radix hexadecimal sim:/tb_lcd_controller/dut_inst/lcd_ctrl_cmd_processor_inst/cmd_wr_data_in
add wave -noupdate -group lcd_cmd_processor -radix unsigned sim:/tb_lcd_controller/dut_inst/lcd_ctrl_cmd_processor_inst/cmd_wr_freespace
add wave -noupdate -group lcd_cmd_processor -radix unsigned sim:/tb_lcd_controller/dut_inst/lcd_ctrl_cmd_processor_inst/cmd_wr_freespace_out
add wave -noupdate -group lcd_cmd_processor sim:/tb_lcd_controller/dut_inst/lcd_ctrl_cmd_processor_inst/hst_wr_en_out
add wave -noupdate -group lcd_cmd_processor -radix hexadecimal sim:/tb_lcd_controller/dut_inst/lcd_ctrl_cmd_processor_inst/hst_wr_data_out
add wave -noupdate -group lcd_cmd_processor sim:/tb_lcd_controller/dut_inst/lcd_ctrl_cmd_processor_inst/hst_wr_ready_in
add wave -noupdate -group lcd_cmd_processor sim:/tb_lcd_controller/dut_inst/lcd_ctrl_cmd_processor_inst/csm1_current_state_reg
add wave -noupdate -group lcd_cmd_processor -radix unsigned sim:/tb_lcd_controller/dut_inst/lcd_ctrl_cmd_processor_inst/data_count
add wave -noupdate -group lcd_cmd_processor -radix unsigned sim:/tb_lcd_controller/dut_inst/lcd_ctrl_cmd_processor_inst/scl_count
add wave -noupdate -group lcd_cmd_processor -expand -group SPI sim:/tb_lcd_controller/dut_inst/lcd_ctrl_cmd_processor_inst/hps_lcm_spim_clk_out
add wave -noupdate -group lcd_cmd_processor -expand -group SPI sim:/tb_lcd_controller/dut_inst/lcd_ctrl_cmd_processor_inst/hps_lcm_spim_mosi_out
add wave -noupdate -group lcd_cmd_processor -expand -group SPI sim:/tb_lcd_controller/dut_inst/lcd_ctrl_cmd_processor_inst/hps_lcm_spim_ss_out
add wave -noupdate -group lcd_cmd_processor -expand -group SPI sim:/tb_lcd_controller/dut_inst/lcd_ctrl_cmd_processor_inst/hps_lcm_d_c_out
add wave -noupdate -group lcd_cmd_processor -expand -group SPI sim:/tb_lcd_controller/dut_inst/lcd_ctrl_cmd_processor_inst/hps_lcm_rst_n_out
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {808000000 fs} 0}
quietly wave cursor active 1
configure wave -namecolwidth 204
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits fs
update
WaveRestoreZoom {535102739 fs} {1060102739 fs}
