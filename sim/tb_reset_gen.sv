// ############################################################################
// File          :  tb_reset_gen.v
// Author(s)     :  Brent Yates
// Email         :  brent.yates@ineoquest.com
// Project       :  N/A
// Organization  :  IneoQuest Technologies Inc.
// Creation Date :  20 Dec 2005
// Description   :  Simple reset generator module for test benches
// ############################################################################
// Functional Description:
//			
//
// ############################################################################
// Exceptions:
// ############################################################################
// Instantiated Blocks:   
// ############################################################################
// Process/Technology specific information:
// ############################################################################
//     ##################################################################
//     #                                                                #
//     #   Copyright (C) 2005 by                                        #
//     #   IneoQuest Technologies Inc.                                  #
//     #   All rights reserved.                                         #
//     #                                                                #
//     #   This document contains proprietary data and is not to be     #
//     #   used, copied, reproduced, stored in a retrieval system,      #
//     #   transmitted, distributed or divulged to unauthorized         #
//     #   persons in whole or in part, without proper authorization    #
//     #   from IneoQuest Technologies Inc.  This information is the    #
//     #   property of IneoQuest Technologies Inc with all rights       #
//     #   reserved.                                                    #
//     #                                                                #
//     ##################################################################

// ------------------------------------------------------------------------------------------------

/**
 * Simple reset gererator for test benches
 *
 **/
module tb_reset_gen
	// Module parameters
	#(
		parameter real RESET_TIME_NS = 200,
		parameter int RESET_ACTIVE_LOW = 0
	 )
	// Module ports
    (
	output logic	reset_out
	);

timeunit 1ns;
timeprecision 1ps;

initial
	begin : INIT_ResetGeneration
	reset_out = ~RESET_ACTIVE_LOW;
	#(RESET_TIME_NS*1ns);
	reset_out = RESET_ACTIVE_LOW;
	end	// end reset generation process

endmodule                                                                                        

