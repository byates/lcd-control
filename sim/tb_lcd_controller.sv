timeunit 1ns;
timeprecision 1ps;

module tb_lcd_controller;

import std_types_pkg::*;
import std_functions_pkg::*;
import lcd_controller_pkg::*;

logic           sys_clock;
logic           sys_reset;

logic   [7:0]   lcd_data_io;
logic           lcd_cs_n_out;
logic           lcd_d_c_out;
logic           lcd_rst_n_out;
logic           lcd_wr_n_out;
logic           lcd_rd_n_out;

lcd_regs_t      avs_address;
logic           avs_read;
logic   [31:0]  avs_writedata;
logic           avs_write;
logic   [31:0]  avs_readdata;

logic           irq_signal;

tb_reset_gen
    #(
        .RESET_TIME_NS(500),
        .RESET_ACTIVE_LOW(FALSE)
     )
    tb_reset_gen_inst
    (
    .reset_out(sys_reset)
    );

tb_clock_gen
    #(
        .FREQUENCY_MHZ(100)
     )
    sys_clock_gen_inst
    (
    .clock_out(sys_clock),
    .clock_90_out()
    );

lcd_controller
    #(
        .PARM_FIFO_DEPTH    (256)
     )
    dut_inst
    (
        .sys_clk_in             (sys_clock),
        .sys_reset_in           (sys_reset),

        .lcd_data_io            (lcd_data_io),
        .lcd_cs_n_out           (lcd_cs_n_out),
        .lcd_d_c_out            (lcd_d_c_out),
        .lcd_rst_n_out          (lcd_rst_n_out),
        .lcd_wr_n_out           (lcd_wr_n_out),
        .lcd_rd_n_out           (lcd_rd_n_out),

        .avs_address_in         (avs_address),
        .avs_read_in            (avs_read),
        .avs_writedata_in       (avs_writedata),
        .avs_write_in           (avs_write),
        .avs_readdata_out       (avs_readdata),

        .interrupt_out          (irq_signal)
    );

task host_write
    (
    input   lcd_regs_t      addr,
    input   uint32_t        data
    );

    avs_address <= addr;
    avs_write <= HIGH;
    avs_writedata <= data;
    @(posedge sys_clock);
    avs_write <= LOW;
    $display("%T: host_write [%s] = 0x%X", $time, addr.name(), data);
    @(posedge sys_clock);
endtask

task host_read
    (
    input   lcd_regs_t      addr,
    output  uint32_t        data
    );
    avs_address <= addr;
    avs_read <= HIGH;
    repeat(2) @(posedge sys_clock);
    avs_read <= LOW;
    data = avs_readdata;
    $display("%T: host_read [%s] = 0x%X", $time, addr.name(), data);
endtask

uint32_t    value;
lcd_data_t  lcd_data;
initial
    begin : MAIN_processor
    $timeformat (-9, 3, " ns", 1);
    avs_read = LOW;
    avs_write = LOW;
    wait(sys_reset === LOW);
    repeat(5) @(posedge sys_clock);

    host_read(.addr(FIFO_COUNTS_REG),   .data(value));
    host_read(.addr(IRQ_STATUS_REG),    .data(value));

    repeat(5) @(posedge sys_clock);

    lcd_data = 0;
    lcd_data.rd_flag = FALSE;
    lcd_data.data_type = CMD;
    lcd_data.data = 8'h10;

    host_write(.addr(FIFO_DATA_REG),    .data(lcd_data));
    @(posedge sys_clock);
    value = 32'h2;
    host_write(.addr(IRQ_SET_EN_REG),   .data(value));
    repeat(10) @(posedge sys_clock);
    host_write(.addr(IRQ_CLR_EN_REG),   .data(value));

    end
endmodule
