
// Module for controlling the LCD on the Arrow SocKit board

module lcd_ctrl_cmd_processor
     // Module parameters
    #(
        parameter int PARM_FIFO_DEPTH = 256
     )
    // Module ports
    (

    // -------------------------------------------------------------------------
    // SYSTEM
    //

    input   logic   sys_clk_in,
    input   logic   sys_reset_in,

    // -------------------------------------------------------------------------
    // COMMANDS FROM HOST
    //

    input   logic                   cmd_wr_en_in,
    input   std_types_pkg::uint32_t cmd_wr_data_in,
    output  std_types_pkg::uint16_t cmd_wr_freespace_out,

    // -------------------------------------------------------------------------
    // COMMANDS TO HOST
    //

    output  logic                   hst_wr_en_out,
    output  std_types_pkg::uint32_t hst_wr_data_out,
    input   logic                   hst_wr_ready_in,

    // -------------------------------------------------------------------------
    // LCD INTERFACE
    //

    inout   logic   [7:0]   lcd_data_io,
    output  logic           lcd_d_c_out,
    output  logic           lcd_wr_n_out,
    output  logic           lcd_rd_n_out,
    output  logic           lcd_cs_n_out,
    output  logic           lcd_rst_n_out

    );

import std_functions_pkg::*;
import std_types_pkg::*;
import lcd_controller_pkg::*;

logic                                   lcd_rd_data_avail;
lcd_data_t                              lcd_rd_data;
lcd_data_t                              lcd_rd_data_reg;
logic                                   lcd_rd_en_reg;
logic   [clogb2(PARM_FIFO_DEPTH) - 1:0] cmd_wr_freespace;

fifo_altera_1clk_ft
    #(
        .PARM_FIFO_DEPTH    (PARM_FIFO_DEPTH),
        .PARM_FIFO_WIDTH    (32),
        .PARM_PAUSE_LEVEL   (PARM_FIFO_DEPTH / 2)
     )
    host_to_lcd_fifo_inst
    (
    .sys_clk_in         (sys_clk_in),
    .sys_reset_in       (sys_reset_in),
    .wr_data_in         (cmd_wr_data_in),
    .wr_en_in           (cmd_wr_en_in),
    .wr_freespace_out   (cmd_wr_freespace),
    .wr_pause_out       (),
    .wr_ready_out       (),
    .rd_data_out        (lcd_rd_data),
    .rd_data_valid_out  (),
    .rd_en_in           (lcd_rd_en_reg),
    .rd_count_out       (),
    .rd_empty_out       (),
    .rd_data_avail_out  (lcd_rd_data_avail)
    );

assign cmd_wr_freespace_out = cmd_wr_freespace;

// ----------------------------------------------------------------------------
// CSM1 LCD TRANSMIT
// ----------------------------------------------------------------------------

typedef enum uint8_t
    {
    CSM1_RESET,
    CSM1_W4_DATA,
    CSM1_START,
    CSM1_LOAD,
    CSM1_WR_WAIT,
    CSM1_WR_HOLD,
    CSM1_WR_DONE,
    CSM1_RD_WAIT,
    CSM1_READ,
    CSM1_RD_HOLD,
    CSM1_RD_DONE
    } csm1_states_t;

csm1_states_t    csm1_current_state_reg    /* synthesis syn_encoding="sequential" */;

always_ff @ (posedge sys_clk_in)
    begin : AFF_CSM1_RegisteredActions

    //---------------------------------------------------------------------
    // Default Assignment To Internals
    //---------------------------------------------------------------------
    lcd_rst_n_out <= HIGH;
    lcd_cs_n_out  <= HIGH;
    lcd_rd_en_reg <= LOW;
    hst_wr_en_out <= LOW;
    //---------------------------------------------------------------------
    // Assignments based on the current state that are DIFFERENT
    // from the default assignments.
    //---------------------------------------------------------------------
    case(csm1_current_state_reg)
    CSM1_RESET:
        begin
        //Initial state during reset.
        lcd_rst_n_out <= LOW;
        csm1_current_state_reg <= CSM1_W4_DATA;
        end
    CSM1_W4_DATA:
        begin
        // wait for data to be available in the fifo
        lcd_wr_n_out <= HIGH;
        lcd_rd_n_out <= HIGH;
        waitCount <= 0;
        if(lcd_rd_data_avail)
            begin
            lcd_rd_data_reg <= lcd_rd_data;
            lcd_rd_en_reg <= HIGH;
            csm1_current_state_reg <= CSM1_START;
            end
        end
    CSM1_START:
        begin
        //
        lcd_d_c_out <= lcd_rd_data_reg.data_type;
        lcd_cs_n_out <= LOW;
        if (lcd_rd_data_reg.rd_flag)
            begin
            lcd_rd_n_out <= LOW;
            end
        else
            begin
            lcd_wr_n_out <= LOW;
            end
        csm1_current_state_reg <= CSM1_LOAD;
        end
    CSM1_LOAD:
        begin
        lcd_cs_n_out <= LOW;
        lcd_data_io <= lcd_rd_data_reg.data;
        if(~lcd_rd_n_out)
            begin
            csm1_current_state_reg <= CSM1_RD_WAIT;
            end
        else
            begin
            csm1_current_state_reg <= CSM1_WR_WAIT;
            end
        end
    CSM1_RD_WAIT:
        begin
        waitCount = waitCount + 1;
        lcd_cs_n_out <= LOW;
        if(waitCount > 36)
            begin
            csm1_current_state_reg <= CSM1_READ;
            lcd_rd_n_out <= HIGH;
            end
        end
    CSM1_READ:
        begin
        lcd_cs_n_out <= LOW;
        waitCount <= 0;
        if(hst_wr_ready_in)
            begin
            hst_wr_data_out <= led_data_io;
            hst_wr_en_out <= HIGH;
            csm1_current_state_reg <= CSM1_RD_HOLD;
            end
        end
    CSM1_RD_HOLD:
        begin
        waitCount <= waitCount + 1;
        if(waitCount > 9)
            begin
            csm1_current_state_reg <= RD_DONE;
            end
        end
    CSM1_RD_DONE:
        begin
        csm1_current_state_reg <= CSM1_W4_DATA;
        end
    CSM1_WR_WAIT:
        begin
        waitCount <= waitCount + 1;
        lcd_cs_n_out <= LOW;
        if(waitCount > 2)
            begin
            csm1_current_state_reg <= CSM1_WR_HOLD;
            lcd_wr_n_out <= HIGH;
            end
        end
    CSM1_WR_HOLD:
        begin
        csm1_current_state_reg <= CSM1_WR_DONE;
        end
    CSM1_WR_DONE:
        begin
        csm1_current_state_reg <= CSM1_W4_DATA;
        end
    endcase
    //Hold state at CSM1_RESET while reset is asserted.
    if (sys_reset_in)
        begin
        //Reset Values
        csm1_current_state_reg <= CSM1_RESET;
        end

    end //end clocked block

endmodule
