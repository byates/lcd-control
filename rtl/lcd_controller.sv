
// Module for controlling the LCD on the Arrow SocKit board

module lcd_controller
     // Module parameters
    #(
        parameter int PARM_FIFO_DEPTH = 256
     )
    // Module ports
    (
    // ---------------------------------------------------------------------------------------------
    // SYSTEM
    //

    input   logic   sys_clk_in,
    input   logic   sys_reset_in,

    // ---------------------------------------------------------------------------------------------
    // LCD INTERFACE
    //

    inout   logic   [7:0]   lcd_data_io,
    output  logic           lcd_d_c_out,
    output  logic           lcd_wr_n_out,
    output  logic           lcd_rd_n_out,
    output  logic           lcd_cs_n_out,
    output  logic           lcd_rst_n_out,

    //--------------------------------------------------------------------------
    // Avalon Slave
    //

    input   logic   [2:0]   avs_address_in,
    input   logic           avs_read_in,
    input   logic   [31:0]  avs_writedata_in,
    input   logic           avs_write_in,
    output  logic   [31:0]  avs_readdata_out,

    //--------------------------------------------------------------------------
    // Output Ports
    //

    output  logic           interrupt_out
    );

import std_functions_pkg::*;
import std_types_pkg::*;
import lcd_controller_pkg::*;

logic           avs_read_pulse_reg;
logic           avs_read_pulse;
logic   [31:0]  avs_writedata_reg;
logic           l2h_rd_en_reg;
logic   [31:0]  l2h_rd_data;
logic   [31:0]  l2h_wr_data_reg;
logic           l2h_wr_ready;
logic           l2h_wr_en_reg;
logic           h2l_wr_en_reg;
logic           l2h_rd_data_avail;
irq_en_t        irq_en_reg;
irq_status_t    irq_stat_reg;
counts_t        fifo_counts;
logic   [clogb2(PARM_FIFO_DEPTH) - 1:0] l2h_rd_count;
lcd_regs_t      reg_address;

assign reg_address = lcd_regs_t'(avs_address_in);

fifo_altera_1clk_ft
    #(
        .PARM_FIFO_DEPTH    (PARM_FIFO_DEPTH),
        .PARM_FIFO_WIDTH    (32),
        .PARM_PAUSE_LEVEL   (4)
     )
    lcd_to_host_fifo_inst
    (
    .sys_clk_in         (sys_clk_in),
    .sys_reset_in       (sys_reset_in),
    .wr_data_in         (l2h_wr_data_reg),
    .wr_en_in           (l2h_wr_en_reg),
    .wr_freespace_out   (),
    .wr_pause_out       (),
    .wr_ready_out       (l2h_wr_ready),
    .rd_data_out        (l2h_rd_data),
    .rd_data_valid_out  (),
    .rd_en_in           (l2h_rd_en_reg),
    .rd_count_out       (l2h_rd_count),
    .rd_empty_out       (),
    .rd_data_avail_out  (l2h_rd_data_avail)
    );

assign fifo_counts.rd_count = l2h_rd_count;

lcd_ctrl_cmd_processor
    #(
        .PARM_FIFO_DEPTH    (256)
     )
    lcd_ctrl_cmd_processor_inst
    (


// readWaitTime is set to 1 which results in a read pulse 2 clocks long. For the FIFO we need just one pulse.
//                                  __    __    __    __    __    __    __    __    __
//      sys_clk_in              \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__
//                                  ___________       _______________________
//      avs_read_in              __/           \_____/                       \_____
//                                  _____             _____       _____
//      avs_read_pulse           __/     \___________/     \_____/     \____
//                                        _____             _____       _____
//      avs_read_pulse_reg       ________/     \___________/     \_____/     \____

assign avs_read_pulse = (avs_read_in && !avs_read_pulse_reg);

always_ff @ (posedge sys_clk_in)
    begin : AFF_regSet
    avs_read_pulse_reg <= avs_read_pulse;
    irq_stat_reg <= 0;
    irq_stat_reg.wr_status <= (fifo_counts.wr_freespace >= (PARM_FIFO_DEPTH / 2));
    irq_stat_reg.rd_status <= (l2h_rd_data_avail);
    irq_stat_reg.wr_status_masked <= ((fifo_counts.wr_freespace >= (PARM_FIFO_DEPTH / 2)) & irq_en_reg.wr_irq_en);
    irq_stat_reg.rd_status_masked <= (l2h_rd_data_avail & irq_en_reg.rd_irq_en);
    end //end clocked process

always_ff @ (posedge sys_clk_in)
    begin : AFF_readRegs
    l2h_rd_en_reg <= ((avs_read_pulse) && (reg_address == FIFO_DATA_REG));
    if (avs_read_pulse)
        begin
        case (reg_address)
            FIFO_DATA_REG:  avs_readdata_out <= l2h_rd_data;
            FIFO_COUNTS_REG: avs_readdata_out <= fifo_counts;
            IRQ_STATUS_REG: avs_readdata_out <= irq_stat_reg;
        endcase
        end
    end //end clocked process

always_ff @ (posedge sys_clk_in)
    begin : AFF_avs_writedata_reg
    avs_writedata_reg <= avs_writedata_in;
    end //end clocked process

always_ff @ (posedge sys_clk_in)
    begin : AFF_writeRegs
    if(sys_reset_in)
        begin
        h2l_wr_en_reg <= LOW;
        irq_en_reg <= 0;
        end
    else
        begin
        h2l_wr_en_reg <= LOW;
        if(avs_write_in)
            begin
            case(reg_address)
                FIFO_DATA_REG:  begin
                                h2l_wr_en_reg <= HIGH;
                                end
                IRQ_SET_EN_REG: begin
                                irq_en_reg <= irq_en_reg | avs_writedata_in;
                                end
                IRQ_CLR_EN_REG: begin
                                irq_en_reg <= irq_en_reg & ~avs_writedata_in;
                                end
            endcase
            end
        end
    end //end clocked process
always_ff @ (posedge sys_clk_in)
    begin : AFF_irqs
    interrupt_out <= (irq_stat_reg.wr_status_masked | irq_stat_reg.rd_status_masked);
    end //end clocked process
endmodule
