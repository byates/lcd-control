// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
// Detailed Description:
//
// Standard package containing standard functions
//
// ------------------------------------------------------------------------------------------------
//
// Instantiated Blocks:
//
// none
//
// ------------------------------------------------------------------------------------------------

/**
 * Standard functions
 *
 **/
package std_functions_pkg;

import std_types_pkg::*;

//-------------------------------------------------------------------------------------------------
// FUNCTION: clogb2
//
// Ceiling of log base 2: i.e., tells us how many bits are needed to hold the value.
//
// Input(s): value to calculate the ceiling of
// Output(s): width in bits needed to hold the value
//
//-------------------------------------------------------------------------------------------------
function automatic int unsigned clogb2(input bit [63:0] value);
    int unsigned i;
    bit [63:0] working_value;
    i = 0;
    working_value = value >> 1;
    while (working_value)
        begin
        i = i + 1;
        working_value = working_value >> 1;
        end
    clogb2 = i + 1;
endfunction


//-------------------------------------------------------------------------------------------------
// FUNCTION: cpowb2
//
// Ceiling of power of base 2: i.e., returns next highest power of two that is equal or larger
// than the passed in value.
//
// Input(s): value to calculate the ceiling of (must be > 0)
// Output(s): next highest power of two >= value
//
//-------------------------------------------------------------------------------------------------
function automatic longint unsigned cpowb2(input longint unsigned value);
    cpowb2 = 2**clogb2(value-1);
endfunction


//-------------------------------------------------------------------------------------------------
// FUNCTION: is_low
//
// Returns true if the value is zero
//
// Input(s): value to check
// Output(s): TRUE or FALSE
//
//-------------------------------------------------------------------------------------------------
function automatic logic is_low(input int value);
    if (value == 0)
        begin
        is_low = TRUE;
        end
    else
        begin
        is_low = FALSE;
        end
endfunction

//-------------------------------------------------------------------------------------------------
// FUNCTION: is_high
//
// Returns true if the value is non-zero
//
// Input(s): value to check
// Output(s): TRUE or FALSE
//
//-------------------------------------------------------------------------------------------------
function automatic logic is_high(input int value);
    if (value != 0)
        begin
        is_high = TRUE;
        end
    else
        begin
        is_high = FALSE;
        end
endfunction

//-------------------------------------------------------------------------------------------------
// FUNCTION: is_false
//
// Returns true if the value is zero
//
// Input(s): value to check
// Output(s): TRUE or FALSE
//
//-------------------------------------------------------------------------------------------------
function automatic logic is_false(input int value);
    if (value == 0)
        begin
        is_false = TRUE;
        end
    else
        begin
        is_false = FALSE;
        end
endfunction

//-------------------------------------------------------------------------------------------------
// FUNCTION: is_true
//
// Returns true if the value is non-zero
//
// Input(s): value to check
// Output(s): TRUE or FALSE
//
//-------------------------------------------------------------------------------------------------
function automatic logic is_true(input int value);
    if (value != 0)
        begin
        is_true = TRUE;
        end
    else
        begin
        is_true = FALSE;
        end
endfunction

//-------------------------------------------------------------------------------------------------
// FUNCTION: max
//
// Returns the larger of two values
//
// Input(s): values to compare
// Output(s): larger of the two values compared
//
//-------------------------------------------------------------------------------------------------
function automatic int max(input int value1, input int value2);
    if (value1 >= value2)
        begin
        max = value1;
        end
    else
        begin
        max = value2;
        end
endfunction

//-------------------------------------------------------------------------------------------------
// FUNCTION: min
//
// Returns the smaller of two values
//
// Input(s): values to compare
// Output(s): smaller of the two values compared
//
//-------------------------------------------------------------------------------------------------
function automatic int min(input int value1, input int value2);
    if (value1 <= value2)
        begin
        min = value1;
        end
    else
        begin
        min = value2;
        end
endfunction

endpackage : std_functions_pkg

