// ------------------------------------------------------------------------------------------------
//
// LCD Controller
//
// ------------------------------------------------------------------------------------------------
//
// Instantiated Blocks:
//
// none
//
// ------------------------------------------------------------------------------------------------

package lcd_controller_pkg;

import std_functions_pkg::*;
import std_types_pkg::*;

typedef enum logic [2:0]
    {
    FIFO_DATA_REG = 3'h0,
    FIFO_COUNTS_REG = 3'h1,
    IRQ_STATUS_REG = 3'h2,
    IRQ_SET_EN_REG = 3'h3,
    IRQ_CLR_EN_REG = 3'h4
    } lcd_regs_t;

// Data type for A0
typedef enum logic
    {
    CMD     = 1'h0,
    DATA    = 1'h1
    } data_type_t;

typedef struct packed
    {
    logic   [31:10] rsvd;
    logic           rd_flag;
    data_type_t     data_type;
    logic   [7:0]   data;
    } lcd_data_t;

typedef struct packed
    {
    logic   [31:2]  rsvd;
    logic           wr_irq_en;
    logic           rd_irq_en;
    } irq_en_t;

typedef struct packed
    {
    logic   [31:18] rsvd;
    logic           wr_status;
    logic           rd_status;
    logic   [15:2]  msk_rsvd;
    logic           wr_status_masked;
    logic           rd_status_masked;
    } irq_status_t;

typedef struct  packed
    {
    logic   [31:16] wr_freespace;
    logic   [15:0]  rd_count;
    } counts_t;

endpackage : lcd_controller_pkg
