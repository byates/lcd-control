// ------------------------------------------------------------------------------------------------
//
// Typedefs for standard types
//
// ------------------------------------------------------------------------------------------------
//
// Instantiated Blocks:
//
// none
//
// ------------------------------------------------------------------------------------------------

/**
 * Typedefs for standard types
 *
 **/
package std_types_pkg;

//-------------------------------------------------------------------------------------------------
// TYPE DEFINITIONS
//-------------------------------------------------------------------------------------------------

localparam logic ZERO = 1'b0;
localparam logic ONE = 1'b1;

// Simple boolean type
localparam logic FALSE = 1'b0;
localparam logic TRUE = 1'b1;

// Simple bitstate type
localparam logic LOW = 1'b0;
localparam logic HIGH = 1'b1;

// signed 4 bit logic type
typedef logic signed   [ 3:0] int4_t;

// unsigned 4 bit logic type
typedef logic unsigned [ 3:0] uint4_t;

// signed 8 bit logic type
typedef logic signed   [ 7:0] int8_t;

// unsigned 8 bit logic type
typedef logic unsigned [ 7:0] uint8_t;

// signed 12 bit logic type
typedef logic signed   [11:0] int12_t;

// unsigned 12 bit logic type
typedef logic unsigned [11:0] uint12_t;

// signed 16 bit logic type
typedef logic signed   [15:0] int16_t;

// unsigned 16 bit logic type
typedef logic unsigned [15:0] uint16_t;

// signed 32 bit logic type
typedef logic signed   [31:0] int32_t;

// unsigned 32 bit logic type
typedef logic unsigned [31:0] uint32_t;

// signed 36 bit logic type
typedef logic signed   [35:0] int36_t;

// unsigned 36 bit logic type
typedef logic unsigned [35:0] uint36_t;

// signed 64 bit logic type
typedef logic signed   [63:0] int64_t;

// unsigned 64 bit logic type
typedef logic unsigned [63:0] uint64_t;

endpackage : std_types_pkg

