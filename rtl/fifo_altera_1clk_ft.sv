// Detailed Description:
//
// Creates a single clock FIFO in "fall-through" mode such that the read data
// is driven BEFORE the read enable.  The width and depth are settable via
// parameters, however, the memory element sizes are fixed.  The scfifo module
// will pick a multiple of one of the memory blocks based on the width and
// depth paramters so pick a width and depth that optimize the block usage.
//
//  Native depths vs widths for a single MLAB memory element.
//  -----------------------------------
//      Depth       Width
//      ------      --------
//      32          20
//      64          10
//
//  Native depths vs widths for a single M20K memory element.
//  -----------------------------------
//      Depth       Width
//      ------      --------
//      16K         1
//      8K          2
//      4K          5
//      2K          10
//      1K          20
//      512         40
//
// Example with the width at 36-bits and the depth at 128 words.
//
//  FIFO Write Cycle
//                         _    __    __    __    __    __    __    __    __    __
//      sys_clk_in          \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__
//                                           _______________________
//      wr_en_in           _________________/                       \________________
//                                           _____ _____ _____ _____
//      wr_data_in         -----------------|_D0__|_D1__|_D2__|_D3__|----------------
//                         _______________________ _____ _____ _____ ________________
//      wr_freespace_out   ___________________128_|_127_|_126_|_125_|_124____________
//                                                       ____________________________
//      rd_data_out        -----------------------------|__D0________________________
//                         _______________________________________________ _____ ____
//      rd_count_out       ________________0______________________________|_1___|_2__
//                         _________________________________________
//      rd_empty_out                                                \________________
//
//  FIFO Read Cycle
//                         _    __    __    __    __    __    __    __    __    __
//      sys_clk_in          \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__
//                                     _____       ___________
//      rd_en_in           ___________/     \_____/           \______________________
//                         _________________ ___________ _____
//      rd_data_out        ____D0___________|_D1________|_D2__|----------------------
//                                     _____       ___________
//      rd_data_valid_out  ___________/     \_____/           \______________________
//                         _________________ ___________ _____ ______________________
//      rd_count_out       _____3___________|_2_________|_1___|_0____________________
//                         ___________________________________
//      rd_data_avail_out                                      \______________________
//                                                             ______________________
//      rd_empty_out       ___________________________________|
//                         ___________________________________ ___________ _____ ____
//      wr_freespace_out   ___________125_____________________|_126_______|_127_|_128
//
// ------------------------------------------------------------------------------------------------
//
// Instantiated Blocks:
//
//    fifo_altera_1clk_ft
//       |
//       |--> scfifo
//
// ------------------------------------------------------------------------------------------------


/**
 * Creates a single clock FIFO in "fall-through" mode
 *
 **/
module fifo_altera_1clk_ft
    // Module parameters
    #(
        // Selects the device family.  Choices are: Cyclone V, Stratix V, etc.
        parameter PARM_INTENDED_DEVICE_FAMILY = "Cyclone V",
        // Selects the memory element type.  Choices are AUTO, M20K, MLAB
        parameter PARM_MEM_TYPE = "RAM_BLOCK_TYPE=AUTO",
        // Sets the width of the fifo.
        parameter int PARM_FIFO_WIDTH = 32,
        // Sets the depth of the fifo.
        parameter int PARM_FIFO_DEPTH = 32,
        // Write pause goes active when the freespace drops equal to or below
        // this value.  NOTE: Delayed by two clock cycles.
        parameter int PARM_PAUSE_LEVEL = 4
     )
    // Module ports
    (
    //--------------------------------------------------------------------------
    // SYSTEM
    //

    // Asychronous reset
    input   logic                                                   sys_clk_in,
    input   logic                                                   sys_reset_in,

    //--------------------------------------------------------------------------
    // WRITE SIDE
    //

    // Write Data
    input   logic   [PARM_FIFO_WIDTH-1:0]                           wr_data_in,
    // Write Enable
    input   logic                                                   wr_en_in,
    // Goes active when the FIFO is almost full (see paramter)
    output  logic   [std_functions_pkg::clogb2(PARM_FIFO_DEPTH)-1:0] wr_freespace_out,
    // Goes active when write should cease due to the FIFO being almost full (see paramter)
    output  logic                                                   wr_pause_out,
    // Goes in-active when write should cease due to the FIFO being almost full (see paramter)
    output  logic                                                   wr_ready_out,

    //--------------------------------------------------------------------------
    // READ SIDE
    //

    // Read Data
    output  logic   [PARM_FIFO_WIDTH-1:0]                           rd_data_out,
    // Asserts along with each valid data word.
    output  logic                                                   rd_data_valid_out,
    // Read Enable
    input   logic                                                   rd_en_in,
    // Read Count: how many words are in the FIFO
    output  logic   [std_functions_pkg::clogb2(PARM_FIFO_DEPTH)-1:0] rd_count_out,
    // Empty out.
    output  logic                                                   rd_empty_out,
    // Data available out.
    output  logic                                                   rd_data_avail_out
    );

import std_types_pkg::*;
import std_functions_pkg::*;

// ------------------------------------------------------------------------------------------------
// Local constants
// ------------------------------------------------------------------------------------------------
localparam RAW_FIFO_COUNT_BITS = clogb2(PARM_FIFO_DEPTH-1);

// ------------------------------------------------------------------------------------------------
// Local signals
// ------------------------------------------------------------------------------------------------
logic   [clogb2(PARM_FIFO_DEPTH)-1:0]   rd_count;

assign rd_data_avail_out = ~rd_empty_out;

assign wr_freespace_out = PARM_FIFO_DEPTH - rd_count_out;

assign rd_data_valid_out = rd_en_in;

// The SCFIFO has a strange glitch where the EMPTY is delayed one clock cycle when going from
// empty to not-empty.  This means the count goes to 1 while empty is still asserted.  The
// sim model complains about this when a read occurs while empty is assertd but count is 1.
// We fix it by gating the count at the beginning with empty.
assign rd_count_out = rd_empty_out ? 0 : rd_count;

// Output a ready and a pause so the user can use the one that makes sense.
assign wr_ready_out = !wr_pause_out;

// ------------------------------------------------------------------------------------------------
// Map FIFO logic to Altera fifo component.
// ------------------------------------------------------------------------------------------------
scfifo
    scfifo_component
    (
    .clock (sys_clk_in),
    .wrreq (wr_en_in),
    .aclr (sys_reset_in),
    .data (wr_data_in),
    .rdreq (rd_en_in),
    .usedw (rd_count[RAW_FIFO_COUNT_BITS-1:0]),
    .empty (rd_empty_out),
    .full (rd_count[RAW_FIFO_COUNT_BITS]),
    .q (rd_data_out),
    .almost_full (wr_pause_out),
    .almost_empty (),
    .sclr (LOW)
    );
defparam
    scfifo_component.add_ram_output_register = "OFF",
    scfifo_component.almost_full_value = (PARM_FIFO_DEPTH - PARM_PAUSE_LEVEL),
    scfifo_component.intended_device_family = PARM_INTENDED_DEVICE_FAMILY,
    scfifo_component.lpm_hint = PARM_MEM_TYPE,
    scfifo_component.lpm_numwords = PARM_FIFO_DEPTH,
    scfifo_component.lpm_showahead = "ON",
    scfifo_component.lpm_type = "scfifo",
    scfifo_component.lpm_width = PARM_FIFO_WIDTH,
    scfifo_component.lpm_widthu = (RAW_FIFO_COUNT_BITS),
    scfifo_component.overflow_checking = "OFF",
    scfifo_component.underflow_checking = "OFF",
    scfifo_component.use_eab = "ON";

endmodule : fifo_altera_1clk_ft
