#
# lcd_controller_sw.tcl
#

create_driver lcd_controller_driver

set_sw_property hw_class_name lcd_controller
set_sw_property version 1.0
set_sw_property min_compatible_hw_version 1.0
set_sw_property auto_initialize true
set_sw_property bsp_subdirectory drivers

add_sw_property include_source HAL/inc/lcd_controller.h
add_sw_property include_source inc/lcd_controller_regs.h
add_sw_property supported_bsp_type hal
add_sw_property supported_bsp_type ucosii
